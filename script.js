const jankenbot = () =>{
    let a = Math.random();
    if(a<=(1/3)){
        return "pedra";
    }
    else if(a<=(2/3)){
        return "tesoura";
    }
    else{
        return "papel";
    }
}

const winnerdetermination = (playerdecision , botdecision)=>{

    if(playerdecision ===botdecision){
        return "empate";
    }
    else if (playerdecision==="pedra"&&botdecision==="papel"){
        return "derrota";
    }
    else if (playerdecision==="tesoura"&&botdecision==="pedra"){
        return "derrota";
    }
    else if (playerdecision==="papel"&&botdecision==="tesoura"){
        return "derrota";
    }
    else{
        return "Vitoria";
    }   

}

// *********************************REFERENCIA*********************************

// for(i=1; i<=3; i++) {
//     // Create a div, with class "bar", and set the width to 100px.
//     var newElement = document.createElement("div");
//     newElement.className = "bar";
//     newElement.style.width = i*100 + "px";

//     // Place a text label inside the new div.
//     var newText = document.createTextNode("Bar #" + i);
//     newElement.appendChild(newText);

//     // Put the new div on the page inside the existing element "d1".
//     var destination = document.getElementById("d1");
//     destination.appendChild(newElement);
//   }

const resultadotela = (playerdecision)=>{
    const resultado_tela=document.getElementById("result");
    while(resultado_tela.firstChild){
        resultado_tela.removeChild(resultado_tela.firstChild)
    }
    let resultado = document.createElement("h2");
    let texto_resultado = document.createTextNode(winnerdetermination(playerdecision,jankenbot()));
    resultado.appendChild(texto_resultado);
    resultado_tela.appendChild(resultado);

}

// const button = document.getElementById('disparador');
// button.onclick = addLorem;
// button.onclick = addOutro;

// button.addEventListener('click', addLorem);
// button.addEventListener('click', addOutro);


// *****************************************************************************



// const container = document.getElementById('buttons_container');
// container.addEventListener('click', function(e) {
//     if (e.target.classList.contains('lorem')) {
//         addLorem();
//     }

//     if (e.target.classList.contains('outro')) {
//         addOutro();
//     }

//     console.log('recebi um clique');
// });

const escolhas = document.getElementById("yourchoice");
escolhas.addEventListener('click',function (botao){
    if(botao.target.classList.contains("stone")){
     resultadotela("pedra");
    }
    else if(botao.target.classList.contains("scissor")){
        resultadotela("tesoura");
    }
    else if(botao.target.classList.contains("paper")){
        resultadotela("papel");
    }
    console.log('recebi um clique');
 });
